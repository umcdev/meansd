use assert_cmd::crate_name;
use assert_cmd::Command;
use predicates::prelude::*;
use std::error::Error;

#[test]
fn empty() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();

    cmd.assert()
        .success()
        .stdout(predicate::str::contains("n=0 ∅ 0 ± 0"));

    Ok(())
}

#[test]
fn single() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();

    cmd.write_stdin("1");

    cmd.assert()
        .success()
        .stdout(predicate::str::contains("n=1 ∅ 1 ± 0"));

    Ok(())
}

#[test]
fn small() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();

    cmd.write_stdin("1 \n 2 \n 3");

    cmd.assert()
        .success()
        .stdout(predicate::str::contains("n=3 ∅ 2 ± 1"));

    Ok(())
}
