use assert_cmd::crate_name;
use assert_cmd::Command;
use predicates::prelude::*;
use std::error::Error;

#[test]
fn meansd() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.args(["--progress", "3"]);

    cmd.write_stdin("1 \n 2 \n 3");

    cmd.assert()
        .success()
        .stderr(predicate::str::contains("progress 3: n=3 ∅ 2 ± 1"))
        .stdout(predicate::str::contains("n=3 ∅ 2 ± 1"));

    Ok(())
}

#[test]
fn binned() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.args(["--progress", "3"]);
    cmd.args(["--bin-width", "5"]);

    cmd.write_stdin("2 \n 3 \n 4 \n 5 \n 6 \n 7 \n");

    cmd.assert()
        .success()
        .stderr(predicate::str::contains("progress 3: 0 - 5 → n=3 ∅ 3 ± 1"))
        .stderr(predicate::str::contains("progress 6: 0 - 5 → n=3 ∅ 3 ± 1"))
        .stderr(predicate::str::contains("progress 6: 5 - 10 → n=3 ∅ 6 ± 1"))
        .stdout(predicate::str::contains("0  5    3    3  1"))
        .stdout(predicate::str::contains("5 10    3    6  1"))
        .stdout(predicate::str::contains("n=6 ∅ 4.5 ± 1.87"));

    Ok(())
}
