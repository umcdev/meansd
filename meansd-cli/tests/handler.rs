use assert_cmd::crate_name;
use assert_cmd::Command;
use predicates::prelude::*;
use std::error::Error;

#[test]
fn warn() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.args(["--error-handler", "warn"]);

    cmd.write_stdin("1 \n 2 \n 3 \n a");

    cmd.assert()
        .success()
        .stderr(predicate::str::contains(
            "warning: invalid float literal: a",
        ))
        .stdout(predicate::str::contains("n=3 ∅ 2 ± 1"));

    Ok(())
}

#[test]
fn skip() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.args(["--error-handler", "skip"]);

    cmd.write_stdin("1 \n 2 \n 3 \n a");

    cmd.assert()
        .success()
        .stderr(predicate::str::is_empty())
        .stdout(predicate::str::contains("n=3 ∅ 2 ± 1"));

    Ok(())
}

#[test]
fn panic() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.args(["--error-handler", "panic"]);

    cmd.write_stdin("1 \n 2 \n 3 \n a");

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("error: invalid float literal: a"))
        .stdout(predicate::str::is_empty());

    Ok(())
}
