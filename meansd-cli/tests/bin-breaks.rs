use assert_cmd::crate_name;
use assert_cmd::Command;
use predicates::prelude::*;
use std::error::Error;

#[test]
fn small() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin(crate_name!()).unwrap();
    cmd.arg("--bin-breaks").arg("0,5,10");

    cmd.write_stdin("2 \n 3 \n 4 \n 5 \n 6 \n 7 \n");

    cmd.assert()
        .success()
        .stdout(predicate::str::contains("0  5    3    3  1"))
        .stdout(predicate::str::contains("5 10    3    6  1"))
        .stdout(predicate::str::contains("n=6 ∅ 4.5 ± 1.87"));

    Ok(())
}
