use std::fmt::Display;

use clap::crate_name;

pub fn progress<S: Display>(n: f64, message: S) {
    eprintln!("{}: progress {}: {}", crate_name!(), n, message);
}

pub fn error<S: Display>(message: S) {
    eprintln!("{}: error: {message}", crate_name!());
}

pub fn warn<S: Display>(message: S) {
    eprintln!("{}: warning: {message}", crate_name!());
}
