meansd
======

Calculates mean and standard deviation.


Usage
-----

Simple execution mode:

```console
$ seq 1 3 | meansd
n=3 ∅ 2 ± 1
```

Binning:

```console
$ seq 2 7 | meansd --bin-width 5

From To Size Mean SD
   0  4    3    3  1
   5  9    3    6  1

n=6 ∅ 5 ± 2
```

For more command line options, see `meansd --help`.


Resource Requirements
---------------------

This tool uses so-called [online algorithms][]. This means that the input is
not stored and you can pipe an indefinite amount to this tool without
increasing its memory requirement.

### Memory

Without binning, `VmPeak` is only **4884 kB**, measured with:

```bash
# generate 2 byte unsigned numbers and pipe to meansd
od -v -A n -t u2 -w2 /dev/urandom |
  meansd --progress 1000000

# measure VmPeak
rg VmPeak /proc/$(pgrep meansd)/status
```

With binning, the memory requirements increase only ever so slightly per bin.

### CPU

During the memory tests, my observation was that `meansd` consumes the numbers
faster than the random number generator can produce them. While `od` was at
100% CPU utilization, `meansd` was at about 50% without binning and 90% with
binning.


[online algorithms]: https://en.wikipedia.org/wiki/Online_algorithm
