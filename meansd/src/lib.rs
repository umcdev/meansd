//! A utility library to calculate mean and standard deviation with online
//! algorithms.
//!
//! # Rationale
//!
//! This library provides ways to calculate the mean and the standard deviation
//! of a *potentially limitless* stream of numbers with *practically no memory
//! consumption*.
//!
//! # Examples
//!
//! ```rust
//! use meansd::MeanSD;
//!
//! let mut meansd = MeanSD::default();
//!
//! meansd.update(1.0);
//! meansd.update(2.0);
//! meansd.update(3.0);
//!
//! assert_eq!(meansd.size(), 3.0);
//! assert_eq!(meansd.mean(), 2.0);
//! assert_eq!(meansd.sstdev(), 1.0);
//! ```

#![forbid(unsafe_code)]
#![deny(clippy::all, missing_docs)]
#![warn(clippy::pedantic, clippy::nursery, clippy::cargo)]

pub mod binned;
mod meansd;

pub use crate::meansd::MeanSD;
