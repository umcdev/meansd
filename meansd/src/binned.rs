//! Online algorithms to calculate mean and standard deviation with binning.
//!
//! # Examples
//!
//! ## Fixed Bin Breaks
//!
//! ```rust
//! use meansd::binned::*;
//!
//! let breaks = vec![0.0, 10.0, 20.0];
//! let mut binned = Breaks::new(&breaks);
//!
//! binned.update(4.0);
//! binned.update(5.0);
//! binned.update(6.0);
//! binned.update(14.0);
//! binned.update(15.0);
//! binned.update(16.0);
//! ```
//!
//! ## Fixed Bin Width
//!
//! ```rust
//! use std::num::NonZeroU32;
//! use meansd::binned::*;
//!
//! let width = NonZeroU32::new(5).unwrap();
//! let mut binned = Width::new(width);
//!
//! binned.update(2.0);
//! binned.update(3.0);
//! binned.update(4.0);
//! binned.update(5.0);
//! binned.update(6.0);
//! binned.update(7.0);
//! ```

use std::collections::BTreeMap;
use std::num::NonZeroU32;

use ordered_float::OrderedFloat;

use crate::MeanSD;

// ----------------------------------------------------------------------------
// bins
// ----------------------------------------------------------------------------

/// A bin.
#[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
pub struct Bin {
    bottom: OrderedFloat<f64>,
    top: OrderedFloat<f64>,
}

impl Bin {
    /// Returns a new bin.
    pub(crate) const fn new(bottom: f64, top: f64) -> Self {
        Self {
            bottom: OrderedFloat(bottom),
            top: OrderedFloat(top),
        }
    }

    /// Returns the bottom end.
    #[must_use]
    pub fn bottom(&self) -> f64 {
        self.bottom.into_inner()
    }

    /// Returns the top end.
    #[must_use]
    pub fn top(&self) -> f64 {
        self.top.into_inner()
    }

    /// Returns `true` if given `number` falls into this bins' boundaries.
    #[must_use]
    pub(crate) fn matches(&self, number: f64) -> bool {
        number >= self.bottom() && number < self.top()
    }
}

// ----------------------------------------------------------------------------
// binned trait
// ----------------------------------------------------------------------------

/// `MeanSD` with binning.
pub trait Binned {
    /// Returns a reference to the stats for all observations.
    fn all(&self) -> &MeanSD;

    /// Returns an iterator over all bins.
    fn bins(&self) -> Box<dyn Iterator<Item = Bin> + '_>;

    /// Returns an iterator over the non-empty bins.
    fn data(&self) -> Box<dyn Iterator<Item = (Bin, &MeanSD)> + '_>;

    /// Updates the data structure with a new observation `x`.
    fn update(&mut self, x: f64);
}

// ----------------------------------------------------------------------------
// binning with breaks
// ----------------------------------------------------------------------------

/// An online algorithm for calculating mean and standard deviation with
/// binning and fixed bin breaks.
///
/// Bins for observations that are out of bounds are automatically created.
///
/// # Resources
///
/// The memory requirement is constant. Although it scales ever so slightly
/// with the amount of breaks, once the data structure is created, it will not
/// grow while adding observations.
#[derive(Clone, Debug)]
pub struct Breaks {
    all: MeanSD,
    data: BTreeMap<Bin, MeanSD>,
}

impl Breaks {
    /// Returns a new and empty data structure with specified breaks.
    #[must_use]
    pub fn new(breaks: &[f64]) -> Self {
        let mut data: BTreeMap<Bin, MeanSD> = BTreeMap::new();

        for (i, x) in breaks.iter().enumerate() {
            let bin = if i == 0 {
                Bin::new(f64::NEG_INFINITY, *x)
            } else {
                Bin::new(breaks[i - 1], *x)
            };

            data.insert(bin, MeanSD::default());
        }

        if let Some(last) = breaks.last() {
            let last_bin = Bin::new(*last, f64::INFINITY);
            data.insert(last_bin, MeanSD::default());
        }

        Self {
            all: MeanSD::default(),
            data,
        }
    }
}

impl Binned for Breaks {
    fn all(&self) -> &MeanSD {
        &self.all
    }

    fn bins(&self) -> Box<dyn Iterator<Item = Bin> + '_> {
        Box::new(self.data.keys().copied())
    }

    fn data(&self) -> Box<dyn Iterator<Item = (Bin, &MeanSD)> + '_> {
        let ret = self.data.iter().filter_map(|(bin, meansd)| {
            if meansd.size() == 0.0 {
                None
            } else {
                Some((*bin, meansd))
            }
        });

        Box::new(ret)
    }

    fn update(&mut self, x: f64) {
        self.all.update(x);

        for (bin, data) in &mut self.data {
            if bin.matches(x) {
                data.update(x);
            }
        }
    }
}

// ----------------------------------------------------------------------------
// binning with fixed width
// ----------------------------------------------------------------------------

/// An online algorithm for calculating mean and standard deviation with
/// binning and a fixed bin width.
///
/// # Resources
///
/// The memory requirement is effectively constant, growing only ever so
/// slightly with each new bin.
#[derive(Clone, Debug)]
pub struct Width {
    all: MeanSD,
    data: BTreeMap<OrderedFloat<f64>, MeanSD>,
    width: NonZeroU32,
}

impl Width {
    /// Returns a new and empty data structure with `width` bin width.
    #[must_use]
    pub fn new(width: NonZeroU32) -> Self {
        Self {
            all: MeanSD::default(),
            data: BTreeMap::new(),
            width,
        }
    }

    /// Returns the bin width.
    #[must_use]
    pub const fn width(&self) -> NonZeroU32 {
        self.width
    }

    fn index_to_bin(&self, index: OrderedFloat<f64>) -> Bin {
        let width = f64::from(self.width.get());

        let bottom = f64::from(self.width.get()) * index.into_inner();
        let top = width * (index.into_inner() + 1.0);

        Bin::new(bottom, top)
    }
}

impl Binned for Width {
    fn update(&mut self, x: f64) {
        self.all.update(x);

        let width = f64::from(self.width.get());
        let bin_index = (x / width).floor();
        let bin_index = OrderedFloat(bin_index);

        self.data.entry(bin_index).or_default().update(x);
    }

    fn bins(&self) -> Box<dyn Iterator<Item = Bin> + '_> {
        Box::new(self.data.keys().map(move |index| self.index_to_bin(*index)))
    }

    fn data(&self) -> Box<dyn Iterator<Item = (Bin, &MeanSD)> + '_> {
        Box::new(
            self.data.iter().map(move |(index, meansd)| {
                (self.index_to_bin(*index), meansd)
            }),
        )
    }

    fn all(&self) -> &MeanSD {
        &self.all
    }
}

// ----------------------------------------------------------------------------
// tests
// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    use float_cmp::assert_approx_eq;

    #[test]
    fn bin_matches() {
        let bin = Bin::new(5.0, 10.0);

        assert!(!bin.matches(0.0));
        assert!(bin.matches(5.0));
        assert!(bin.matches(9.9));
        assert!(!bin.matches(10.0));
    }

    #[test]
    fn binned_breaks() {
        let empty = Breaks::new(&[]);
        assert_eq!(0, empty.bins().count());
        assert_eq!(0, empty.data().count());

        let mut binned = Breaks::new(&[10.0, 20.0]);
        assert_eq!(3, binned.bins().count());

        binned.update(4.0);
        binned.update(5.0);
        binned.update(6.0);
        binned.update(14.0);
        binned.update(15.0);
        binned.update(16.0);

        let mut data = binned.data();

        let (bin_a, meansd_a) = data.next().unwrap();

        assert_eq!(Bin::new(f64::NEG_INFINITY, 10.0), bin_a);
        assert_approx_eq!(f64, 3.0, meansd_a.size());
        assert_approx_eq!(f64, 5.0, meansd_a.mean());
        assert_approx_eq!(f64, 1.0, meansd_a.sstdev());

        let (bin_b, meansd_b) = data.next().unwrap();

        assert_eq!(Bin::new(10.0, 20.0), bin_b);
        assert_approx_eq!(f64, 3.0, meansd_b.size());
        assert_approx_eq!(f64, 15.0, meansd_b.mean());
        assert_approx_eq!(f64, 1.0, meansd_b.sstdev());

        // third bin is excluded from `fn data()` because it is empty
        assert!(data.next().is_none());

        let all = binned.all();

        assert_approx_eq!(f64, 6.0, all.size());
        assert_approx_eq!(f64, 10.0, all.mean());
        assert_approx_eq!(f64, 5.549_774_770_204_643, all.sstdev());
    }

    #[test]
    fn binned_width() {
        let width = NonZeroU32::new(5).unwrap();
        let mut binned = Width::new(width);
        assert_eq!(5, binned.width().get());

        assert_eq!(0, binned.bins().count());

        binned.update(2.0);
        binned.update(3.0);
        binned.update(4.0);

        assert_eq!(1, binned.bins().count());

        binned.update(5.0);
        binned.update(6.0);
        binned.update(7.0);

        assert_eq!(2, binned.bins().count());

        let mut data = binned.data();

        let (bin_a, meansd_a) = data.next().unwrap();

        assert_eq!(Bin::new(0.0, 5.0), bin_a);
        assert_approx_eq!(f64, 3.0, meansd_a.size());
        assert_approx_eq!(f64, 3.0, meansd_a.mean());
        assert_approx_eq!(f64, 1.0, meansd_a.sstdev());

        let (bin_b, meansd_b) = data.next().unwrap();

        assert_eq!(Bin::new(5.0, 10.0), bin_b);
        assert_approx_eq!(f64, 3.0, meansd_b.size());
        assert_approx_eq!(f64, 6.0, meansd_b.mean());
        assert_approx_eq!(f64, 1.0, meansd_b.sstdev());

        assert!(data.next().is_none());

        let all = binned.all();

        assert_approx_eq!(f64, 6.0, all.size());
        assert_approx_eq!(f64, 4.5, all.mean());
        assert_approx_eq!(f64, 1.870_828_693_386_970_7, all.sstdev());
    }
}
