/// An online algorithm for calculating mean and standard deviation.
///
/// # Algorithm
///
/// [Welford's Online Algorithm][1] is used.
///
/// # Resources
///
/// The memory requirement is constant.
///
/// [1]: https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
#[derive(Clone, Copy, Debug, Default)]
pub struct MeanSD {
    size: f64,
    mean: f64,
    m2: f64,
}

impl MeanSD {
    /// Updates the data structure with a new observation `x`.
    pub fn update(&mut self, x: f64) {
        let delta = x - self.mean;

        self.size += 1.0;
        self.mean += delta / self.size;
        self.m2 += delta * (x - self.mean);
    }

    /// Returns the number of observations.
    #[must_use]
    pub const fn size(&self) -> f64 {
        self.size
    }

    /// Returns the mean.
    #[must_use]
    pub const fn mean(&self) -> f64 {
        self.mean
    }

    /// Returns the population standard deviation.
    #[must_use]
    pub fn pstdev(&self) -> f64 {
        if self.size > 1.0 {
            let variance = self.m2 / self.size;
            variance.sqrt()
        } else {
            0.0
        }
    }

    /// Returns the sample standard deviation.
    #[must_use]
    pub fn sstdev(&self) -> f64 {
        if self.size > 1.0 {
            let variance = self.m2 / (self.size - 1.0);
            variance.sqrt()
        } else {
            0.0
        }
    }

    /// Returns the population standard error of the mean.
    #[must_use]
    pub fn psem(&self) -> f64 {
        if self.size == 0.0 {
            0.0
        } else {
            self.pstdev() / self.size.sqrt()
        }
    }

    /// Returns the sample standard error of the mean.
    #[must_use]
    pub fn ssem(&self) -> f64 {
        if self.size == 0.0 {
            0.0
        } else {
            self.sstdev() / self.size.sqrt()
        }
    }
}

// ----------------------------------------------------------------------------
// tests
// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    use float_cmp::assert_approx_eq;

    #[test]
    fn empty() {
        let meansd = MeanSD::default();

        assert_approx_eq!(f64, 0.0, meansd.size());
        assert_approx_eq!(f64, 0.0, meansd.mean());
        assert_approx_eq!(f64, 0.0, meansd.sstdev());
        assert_approx_eq!(f64, 0.0, meansd.pstdev());
        assert_approx_eq!(f64, 0.0, meansd.ssem());
        assert_approx_eq!(f64, 0.0, meansd.psem());
    }

    #[test]
    fn single() {
        let mut meansd = MeanSD::default();

        meansd.update(1.0);

        assert_approx_eq!(f64, 1.0, meansd.size());
        assert_approx_eq!(f64, 1.0, meansd.mean());
        assert_approx_eq!(f64, 0.0, meansd.sstdev());
        assert_approx_eq!(f64, 0.0, meansd.pstdev());
        assert_approx_eq!(f64, 0.0, meansd.ssem());
        assert_approx_eq!(f64, 0.0, meansd.psem());
    }

    #[test]
    fn small_positive() {
        let mut meansd = MeanSD::default();

        meansd.update(1.0);
        meansd.update(2.0);
        meansd.update(3.0);

        assert_approx_eq!(f64, 3.0, meansd.size());
        assert_approx_eq!(f64, 2.0, meansd.mean());
        assert_approx_eq!(f64, 1.0, meansd.sstdev());
        assert_approx_eq!(f64, 0.577_350_269_189_625_8, meansd.ssem());
    }

    #[test]
    fn small_negative() {
        let mut meansd = MeanSD::default();

        meansd.update(-1.0);
        meansd.update(-2.0);
        meansd.update(-3.0);

        assert_approx_eq!(f64, 3.0, meansd.size());
        assert_approx_eq!(f64, -2.0, meansd.mean());
        assert_approx_eq!(f64, 1.0, meansd.sstdev());
        assert_approx_eq!(f64, 0.577_350_269_189_625_8, meansd.ssem());
    }

    #[test]
    fn small_mixed() {
        let mut meansd = MeanSD::default();

        meansd.update(-1.0);
        meansd.update(0.0);
        meansd.update(1.0);

        assert_approx_eq!(f64, 3.0, meansd.size());
        assert_approx_eq!(f64, 0.0, meansd.mean());
        assert_approx_eq!(f64, 1.0, meansd.sstdev());
        assert_approx_eq!(f64, 0.577_350_269_189_625_8, meansd.ssem());
    }

    #[test]
    fn population() {
        let mut meansd = MeanSD::default();

        meansd.update(2.0);
        meansd.update(4.0);
        meansd.update(4.0);
        meansd.update(4.0);
        meansd.update(5.0);
        meansd.update(5.0);
        meansd.update(7.0);
        meansd.update(9.0);

        assert_approx_eq!(f64, 8.0, meansd.size());
        assert_approx_eq!(f64, 5.0, meansd.mean());
        assert_approx_eq!(f64, 2.0, meansd.pstdev());
        assert_approx_eq!(f64, 0.707_106_781_186_547_5, meansd.psem());
    }
}
